FROM tomcat

MAINTAINER Rajat
RUN rm -f /usr/local/tomcat/webapps/demosampleapplication_4750.war
RUN wget -O /usr/local/tomcat/webapps/demosampleapplication_4750.war --user admin --password admin  http://10.127.126.113:8081/artifactory/rajatgoyal.4750/com/nagarro/devops-tools/devops/demosampleapplication/1.0.0-SNAPSHOT/demosampleapplication-1.0.0-SNAPSHOT.war

CMD ["run"]
ENTRYPOINT ["/usr/local/tomcat/bin/catalina.sh"]

EXPOSE 8080
