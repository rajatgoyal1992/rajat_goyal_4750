package com.devopssample.controller;

import java.util.Random;

public final class TaskHelper{
	
	public static int generateRandomNumber() {
		Random rand = new Random();
		int generatedNumber = rand.nextInt(10);
		return generatedNumber;
	}
	
	public static boolean compare(int generatedNumber) {
		String str1 = "Random number generated is lower than 5. You Lose!";
		String str2 = "Random number generated is greater than 5. You Won!";
		if(generatedNumber <= 5) {
			System.out.println(str1);
			return false;
		} else {
			System.out.println(str2);
			return true;
		}
	}
	
}